default: help

include common.mk

DNS		?= 128.224.200.11
DOCKER_ID_USER	?= wallinux

################################################################

-include network.mk
include openldap.mk

pull:: # Update all images
	$(TRACE)

docker.rm: # Remove all dangling containers
	$(TRACE)
	$(DOCKER) ps -qa --filter "status=exited" | xargs docker rm

docker.rmi: # Remove all dangling images
	$(TRACE)
	$(DOCKER) images -q -f dangling=true | xargs docker rmi

clean::
	$(RM) -r $(STAMPSDIR)
	$(RM) *~

docker.help:
	$(call run-help, Makefile)

help:: docker.help # Show available rules and info about them
	$(TRACE)
